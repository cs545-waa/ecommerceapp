package miu.edu.cs545.eCommerceApp.repository;

import miu.edu.cs545.eCommerceApp.domain.Review;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReviewRepository extends JpaRepository<Review, Long> {
}
