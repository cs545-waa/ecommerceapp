package miu.edu.cs545.eCommerceApp.repository;

import miu.edu.cs545.eCommerceApp.domain.Person;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonRepository extends JpaRepository<Person, Long> {
}
