package miu.edu.cs545.eCommerceApp.repository;

import miu.edu.cs545.eCommerceApp.domain.Order;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Order, Long> {
}
