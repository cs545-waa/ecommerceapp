package miu.edu.cs545.eCommerceApp.repository;

import miu.edu.cs545.eCommerceApp.domain.Cart;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CartRepository extends JpaRepository<Cart, Long> {
}
