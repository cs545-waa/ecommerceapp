package miu.edu.cs545.eCommerceApp.repository;

import miu.edu.cs545.eCommerceApp.domain.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Long> {
}
