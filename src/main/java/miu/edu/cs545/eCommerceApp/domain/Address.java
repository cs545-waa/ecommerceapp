package miu.edu.cs545.eCommerceApp.domain;

import lombok.Data;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Embeddable
@Data
public class Address {
    @NotNull(message = "street field is required")
    @NotBlank(message = "street can't be blank")
    private String street;
    @NotNull(message = "zip field is required")
    @NotBlank(message = "zip can't be blank")
    private String zip;
    @NotNull(message = "city field is required")
    @NotBlank(message = "city can't be blank")
    private String city;
    @NotNull(message = "state field is required")
    @NotBlank(message = "state can't be blank")
    private String state;
}
