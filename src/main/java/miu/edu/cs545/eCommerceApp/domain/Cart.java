package miu.edu.cs545.eCommerceApp.domain;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Entity
@Data
public class Cart {

    @Id
    @GeneratedValue
    private Long id;

    private boolean isOrdered = false;

    @NotNull(message = "amount field is required")
    private BigDecimal amount;

    @OneToOne
    @JoinColumn(name = "buyer_id")
    private Person buyer;
}
