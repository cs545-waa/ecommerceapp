package miu.edu.cs545.eCommerceApp.domain;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Data
public class Review {

    @Id
    @GeneratedValue
    private Long id;

    @NotNull(message = "rating field is required")
    private String rating;

    @NotNull(message = "comment field is required")
    private String comment;
}
