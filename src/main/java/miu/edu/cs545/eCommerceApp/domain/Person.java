package miu.edu.cs545.eCommerceApp.domain;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity
@Data
public class Person {

    @Id
    @GeneratedValue
    private Long id;

    @NotNull(message = "firstName field is required")
    @NotBlank(message = "zip can't be blank")
    private String firsName;
    @NotNull(message = "lastName field is required")
    @NotBlank(message = "zip can't be blank")
    private String lastName;
    @NotNull(message = "email field is required")
    @NotBlank(message = "email can't be blank")
    @Email
    private String email;
    @NotNull(message = "password field is required")
    @NotBlank(message = "password can't be blank")
    private String password;

    @NotNull(message = "role field is required")
    private Role role;

    @NotNull
    @ManyToMany
    @JoinTable(name = "seller_followers")
    Set<Person> follower;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "street", column = @Column(name="bill_street")),
            @AttributeOverride(name = "zip", column = @Column(name="bill_zip")),
            @AttributeOverride(name = "city", column = @Column(name="bill_city")),
            @AttributeOverride(name = "state", column = @Column(name="bill_state"))
    })
    private Address billing_address;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "street", column = @Column(name="ship_street")),
            @AttributeOverride(name = "zip", column = @Column(name="ship_zip")),
            @AttributeOverride(name = "city", column = @Column(name="ship_city")),
            @AttributeOverride(name = "state", column = @Column(name="ship_state"))
    })
    private Address shipping_address;

}
