package miu.edu.cs545.eCommerceApp.domain;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Data
public class Order {

    @Id
    @GeneratedValue
    private Long id;

    @NotNull(message = "amount field is required")
    private Status status;

    @OneToOne
    @JoinColumn(name = "cart_id")
    private Cart cart;
}
