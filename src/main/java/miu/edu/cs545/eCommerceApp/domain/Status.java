package miu.edu.cs545.eCommerceApp.domain;

public enum Status {
    CANCELLED,
    PURCHASED,
    SHIPPED,
    ON_THE_WAY,
    DELIVERED;
}
