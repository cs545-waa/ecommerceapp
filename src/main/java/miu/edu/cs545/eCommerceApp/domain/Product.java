package miu.edu.cs545.eCommerceApp.domain;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

@Entity
@Data
public class Product {

    @Id
    @GeneratedValue
    private Long id;

    @OneToOne
    @JoinColumn(name = "seller_id")
    private Person seller;

    @NotNull(message = "price field is required")
    private BigDecimal price;

    @OneToMany
    @JoinColumn(name = "product_id")
    private List<Review> products;
}
