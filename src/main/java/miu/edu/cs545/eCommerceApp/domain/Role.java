package miu.edu.cs545.eCommerceApp.domain;

public enum Role {
    ADMIN,
    SELLER,
    BUYER
}
