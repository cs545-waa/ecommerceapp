package miu.edu.cs545.eCommerceApp.service;

import java.util.List;

public interface CartService {
    List<String> addCartProductsByBuyerId(Long buyerId, String product);
    void deleteCart(Long buyerId);
    List<String> getCartProductsByBuyerId(Long buyerId);
}
