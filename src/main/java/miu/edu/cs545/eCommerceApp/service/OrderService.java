package miu.edu.cs545.eCommerceApp.service;

import java.util.Optional;

public interface OrderService {
    Optional<String> getByOrderId(Long id);
    Iterable<String> getOrdersByBuyerId(Long buyerId);
    Optional<String> addOrder(String newOrder);
}
