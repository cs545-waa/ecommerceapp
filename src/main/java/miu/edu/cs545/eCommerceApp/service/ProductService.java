package miu.edu.cs545.eCommerceApp.service;


import java.util.Optional;


public interface ProductService {
    Iterable<String> getAllProducts();
    Optional<String> getProduct(Long id);

}
