package miu.edu.cs545.eCommerceApp.service;

import java.util.List;

public interface ReviewService {
    String addRating(String productId);
    String addCommentByProductId(Long productId);
    List<String> getRatingByProductId(String productId);
}
